starjava-votable (2.0+2024.08.29-1) unstable; urgency=medium

  * New upstream version 2.0+2024.08.29

 -- Ole Streicher <olebole@debian.org>  Tue, 19 Nov 2024 12:53:33 +0100

starjava-votable (2.0+2024.08.05-1) unstable; urgency=medium

  * New upstream version 2.0+2024.08.05
  * Rediff patches
  * Update minversion of starlink-fits-java dependency

 -- Ole Streicher <olebole@debian.org>  Sat, 12 Oct 2024 14:23:52 +0200

starjava-votable (2.0+2024.02.22-1) unstable; urgency=medium

  * Update get-orig-source to use git sparse checkout instead of svn
  * New upstream version 2.0+2024.02.22
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Thu, 23 May 2024 12:16:12 +0200

starjava-votable (2.0+2023.10.31-1) unstable; urgency=medium

  * New upstream version 2.0+2023.10.31
  * New dependency starlink-auth-java
  * Update minversion of starlink-util-java build dependency

 -- Ole Streicher <olebole@debian.org>  Thu, 09 Nov 2023 16:45:01 +0100

starjava-votable (2.0+2023.04.14-1) unstable; urgency=medium

  * New upstream version 2.0+2023.04.14
  * Remove unneeded versionized breaks
  * Push Standards-Version to 4.6.2. No changes needed.
  * Update starjava-table build-dep minversion

 -- Ole Streicher <olebole@debian.org>  Mon, 26 Jun 2023 09:31:30 +0200

starjava-votable (2.0+2022.08.30-1) unstable; urgency=medium

  * New upstream version 2.0+2022.08.30
  * Push minversion of starjava-table to 4.1.3
  * Push minversion of starjava-util to 1.0+2022.09.23
  * Push Standards-Version to 4.6.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Fri, 07 Oct 2022 12:13:56 +0200

starjava-votable (2.0+2022.04.04-1) unstable; urgency=medium

  * New upstream version 2.0+2022.04.04
  * Rediff patches
  * Update minversion of starjava (build) dependencies

 -- Ole Streicher <olebole@debian.org>  Fri, 22 Apr 2022 14:22:01 +0200

starjava-votable (2.0+2022.01.28-1) unstable; urgency=medium

  * New upstream version 2.0+2022.01.28

 -- Ole Streicher <olebole@debian.org>  Mon, 31 Jan 2022 14:51:54 +0100

starjava-votable (2.0+2021.10.15-2) unstable; urgency=medium

  * Fix versionized dependency of starjava-table

 -- Ole Streicher <olebole@debian.org>  Mon, 10 Jan 2022 08:11:00 +0100

starjava-votable (2.0+2021.10.15-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Ole Streicher ]
  * New upstream version 2.0+2021.10.15
  * Push Standards-Version to 4.6.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 04 Jan 2022 15:52:51 +0100

starjava-votable (2.0+2021.01.10-2) unstable; urgency=medium

  * Add versioned dependencies for starlink-table and -util

 -- Ole Streicher <olebole@debian.org>  Thu, 04 Feb 2021 22:12:29 +0100

starjava-votable (2.0+2021.01.10-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Replace use of deprecated $ADTTMP with $AUTOPKGTEST_TMP.

  [ Ole Streicher ]
  * New upstream version 2.0+2021.01.10
  * Rediff patches
  * Push Standards-Version to 4.5.1. No changes needed
  * Add versionized build-deps for starlink-table and -util

 -- Ole Streicher <olebole@debian.org>  Fri, 15 Jan 2021 10:56:38 +0100

starjava-votable (2.0+2020.04.02-2) unstable; urgency=low

  * Fix Depends and Breaks versions across starjava
  * Add Rules-Requires-Root: no

 -- Ole Streicher <olebole@debian.org>  Wed, 24 Jun 2020 20:24:58 +0200

starjava-votable (2.0+2020.04.02-1) unstable; urgency=low

  * New upstream version 2.0+2020.04.02
  * Push Standards-Version to 4.5.0. No changes needed
  * Push dh compat to 13
  * Remove outdated build-dep minversions

 -- Ole Streicher <olebole@debian.org>  Mon, 22 Jun 2020 20:51:59 +0200

starjava-votable (2.0+2019.11.15-1) unstable; urgency=low

  * New upstream version 2.0+2019.11.15
  * Push Standards-Version to 4.4.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Fri, 22 Nov 2019 13:06:53 +0100

starjava-votable (2.0+2019.07.12-2) unstable; urgency=low

  * Fix min version for starjava-table

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 20:35:29 +0200

starjava-votable (2.0+2019.07.12-1) unstable; urgency=low

  * New upstream version 2.0+2019.07.12. Rediff patches
  * Push Standards-Version to 4.4.0. No changes required
  * Push compat to 12. Remove d/compat
  * Add gitlab-ci for salsa

 -- Ole Streicher <olebole@debian.org>  Fri, 02 Aug 2019 10:55:00 +0200

starjava-votable (2.0+2018.10.31-2) unstable; urgency=low

  * Make starjava-votable reproducible

 -- Ole Streicher <olebole@debian.org>  Sat, 17 Nov 2018 21:41:00 +0100

starjava-votable (2.0+2018.10.31-1) unstable; urgency=medium

  * New upstream version 2.0+2018.10.31. Closes: #912346
  * Push Standards-Version to 4.2.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Thu, 01 Nov 2018 09:34:59 +0100

starjava-votable (2.0+2018.05.01-1) unstable; urgency=low

  * New upstream version 2.0+2018.05.01

 -- Ole Streicher <olebole@debian.org>  Thu, 14 Jun 2018 11:30:35 +0200

starjava-votable (2.0+2018.04.17-2) unstable; urgency=low

  * Add versioned depends to java package (Closes: #896977)

 -- Ole Streicher <olebole@debian.org>  Thu, 26 Apr 2018 16:29:09 +0200

starjava-votable (2.0+2018.04.17-1) unstable; urgency=low

  * Update VCS fields to use salsa.d.o
  * New upstream version 2.0+2018.04.17. Rediff patches
  * Push Standards-Version to 4.1.4. No changes needed.
  * Push compat to 11
  * Check DEB_BUILD_OPTIONS on  override_dh_auto_test

 -- Ole Streicher <olebole@debian.org>  Tue, 24 Apr 2018 15:47:10 +0200

starjava-votable (2.0+2017.11.07-1) unstable; urgency=low

  * New upstream version 2.0+2017.11.07

 -- Ole Streicher <olebole@debian.org>  Wed, 08 Nov 2017 22:02:58 +0100

starjava-votable (2.0+2017.07.31-1) unstable; urgency=low

  * New upstream version 2.0+2017.07.31
  * Set doc package to Multi-Arch: foreign
  * Rediff patches
  * Push Standards-Version to 4.1.1 (no changes)

 -- Ole Streicher <olebole@debian.org>  Thu, 05 Oct 2017 16:11:29 +0200

starjava-votable (2.0+2016.12.22-1) unstable; urgency=low

  * Initial release. (Closes: #855792)

 -- Ole Streicher <olebole@debian.org>  Mon, 10 Apr 2017 20:33:12 +0200
